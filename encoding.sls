;; -*- mode: scheme; coding: utf-8 -*-
;; Copyright (c) 2023 
;; SPDX-License-Identifier: MIT
#!r6rs

(library (vterm encoding)
  (export
    make-ascii-decoder
    decode-text)
  (import (rnrs))

(define-record-type vterm-decoder
  (fields name func)
  (protocol
    (lambda (new)
      (lambda (name func)
        (new name func)))))

;; returns (values '(code points) bytes-count)
(define decode-text
  (case-lambda
    [(dec bv)
     ((vterm-decoder-func dec) bv 0 (bytevector-length bv))]
    [(dec bv i len)
     ((vterm-decoder-func dec) bv i len)] ))

(define (make-ascii-decoder)
  (make-vterm-decoder
    'ascii
    (lambda (bv pos len)
      (let loop ([cpl '()] [bc 0] [i pos])
        (if (< i (+ pos len))
            (let* ([byte (bytevector-u8-ref bv i)]
                   [byte (bitwise-xor byte (bitwise-and #x80 byte))])
              (if (or (< byte #x20) (= byte #x7f) (>= byte #x80))
                  (values cpl bc)
                  (loop (append cpl (list byte)) (+ bc 1) (+ i 1) )))
            (let ()
              (values cpl bc)))) )))
)
