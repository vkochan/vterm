;; -*- mode: scheme; coding: utf-8 -*-
;; Copyright (c) 2023 
;; SPDX-License-Identifier: MIT
#!r6rs

(library (vterm state)
  (export
    make-vterm-state
    vterm-state-process
    vterm-state-cursor-col
    vterm-state-cursor-row)
  (import
    (rnrs (6))
    (vterm parser))

(define-record-type vterm-pos
  (fields
    (mutable col)
    (mutable row)))

(define (vterm-pos=? pos1 pos2)
  (and (= (vterm-pos-col pos1)
          (vterm-pos-col pos2))
       (= (vterm-pos-row pos1)
          (vterm-pos-row pos2))))

(define-record-type vterm-rect
  (fields start-col start-row end-col end-row))

(define-record-type vterm-style
  (fields
    (mutable fg)
    (mutable bg)
    (mutable attr)))

(define-enumeration line-info-state
  (double-width
   double-height-top
   double-height-bottom)
  make-line-info-state)

(define-enumeration vterm-mode-state
  (keypad
   cursor
   auto-wrap
   insert
   newline
   cursor-visible
   cursor-blink
   cursor-shape
   alt-screen
   origin
   screen
   left-right-margin
   bracket-paste
   report_focus)
  make-vterm-mode-state)

(define-syntax lbound
  (lambda (stx)
    (syntax-case stx ()
      [(_ var min)
       (identifier? #'var)
       #'(when (< var min)
           (set! var min))] )))

(define-syntax ubound
  (lambda (stx)
    (syntax-case stx ()
      [(_ var max)
       (identifier? #'var)
       #'(when (> var max)
           (set! var max))] )))

(define (make-list n x)
  (if (= n 0)
      '()
      (cons x (make-list (- n 1) x))))

(define (make-tab-stops cols)
  (let ([tabs (make-bytevector (fxdiv (+ cols 7) 8))])
    (let loop ([i 0])
      (when (< i cols)
        (if (zero? (mod i 8))
            (set-col-tab-stop tabs i)
            (clear-col-tab-stop tabs i))
        (loop (+ i 1))))
    tabs))

(define (tab-stop-mask col)
  (fxarithmetic-shift-left 1 (bitwise-and col 7)))

(define (tab-stop-idx col)
  (fxarithmetic-shift-right col 3))

(define (set-col-tab-stop tabs col)
  (let* ([idx (tab-stop-idx col)]
         [mask (tab-stop-mask col)]
         [val (bytevector-u8-ref tabs idx)])
    (bytevector-u8-set! tabs idx (bitwise-ior val mask))))

(define (clear-col-tab-stop tabs col)
  (let* ([idx (tab-stop-idx col)]
         [mask (tab-stop-mask col)]
         [val (bytevector-u8-ref tabs idx)])
    (bytevector-u8-set! tabs idx (bitwise-and val (bitwise-not mask)))))

(define (is-col-tab-stop? tabs col)
  (let* ([idx (tab-stop-idx col)]
         [mask (tab-stop-mask col)]
         [val (bytevector-u8-ref tabs idx)])
    (positive? (bitwise-and val mask))))

(define-record-type (vterm-state make-$vterm-state vterm-state?)
  (fields
    (mutable rows)
    (mutable cols)
    (mutable cursor)
    (mutable at-phantom?)
    (mutable scroll-top)
    (mutable scroll-bottom)
    (mutable scroll-left)
    (mutable scroll-right)
    (mutable tab-stops)
    (mutable line-info)
    (mutable mode)
    (mutable gl)
    (mutable gr)
    (mutable gsingle)
    (mutable style)
    (mutable default-fg)
    (mutable default-bg)
    (mutable colors)
    (mutable parser)
    (mutable handler))
  (protocol
    (lambda (new)
      (lambda ()
        (new 25 80 (make-vterm-pos 0 0)
             #f  ;; at-phantom?
             0 -1 ;; scroll-top, scroll-bottom
             0 -1 ;; scroll-left, scroll-right
             (make-tab-stops 80) ;; tab-stops
             (make-list 25 (make-line-info-state)) ;; line-info
             (make-vterm-mode-state auto-wrap) ;; mode
             0  ;; gl-set
             1  ;; gr-set
             0  ;; gsingle-set
             (make-vterm-style 0 0 0)
             0 0 ;; default-fg, default-bg
             '() ;; colors
             #f  ;; parser
             #f  ;; handler
             )))))

(define (vterm-state-cursor-col state)
  (vterm-pos-col (vterm-state-cursor state)))

(define (vterm-state-cursor-row state)
  (vterm-pos-row (vterm-state-cursor state)))

(define (vterm-state-cursor-col-set! state col)
  (vterm-pos-col-set! (vterm-state-cursor state) col))

(define (vterm-state-cursor-row-set! state row)
  (vterm-pos-row-set! (vterm-state-cursor state) row))

(define (vterm-state-cursor-col-inc! state inc)
  (vterm-pos-col-set! (vterm-state-cursor state)
                      (+ (vterm-state-cursor-col state) inc)))

(define (vterm-state-cursor-row-inc! state inc)
  (vterm-pos-row-set! (vterm-state-cursor state)
                      (+ (vterm-state-cursor-row state) inc)))

(define-record-type vterm-state-event)

(define-record-type vterm-state-glyph-event
  (parent vterm-state-event)
  (fields char width pos))

(define-record-type vterm-state-cursor-event
  (parent vterm-state-event)
  (fields new-pos old-pos visible?))

(define-record-type vterm-state-move-event
  (parent vterm-state-event)
  (fields dst src))

(define-record-type vterm-state-scroll-event
  (parent vterm-state-event)
  (fields rect downward rightward))

(define-record-type vterm-state-erase-event
  (parent vterm-state-event)
  (fields rect selective?))

(define-record-type vterm-state-style-event
  (parent vterm-state-event)
  (fields style))

(define-record-type vterm-state-property-event
  (parent vterm-state-event)
  (fields key value))

(define-record-type vterm-state-bell-event
  (parent vterm-state-event))

(define-record-type vterm-state-resize-event
  (parent vterm-state-event)
  (fields cols rows))

(define-record-type vterm-state-event-line-info
  (parent vterm-state-event)
  (fields row new old))

(define-record-type $vterm-state-handler
  (fields parser state handler))

(define (vterm-state-row-line-info state row)
  (list-ref (vterm-state-line-info state) row))

(define (vterm-state-row-width state row)
  (let ([line-info (vterm-state-row-line-info state row)])
    (if (enum-set-member? 'double-width line-info)
        (fxdiv (vterm-state-cols state) 2)
        (vterm-state-cols state))))

(define (vterm-state-current-row-width state)
  (vterm-state-row-width state (vterm-state-cursor-row state)))

(define (vterm-state-has-mode? state mode)
  (enum-set-member? mode (vterm-state-mode state)))

(define (scroll-region-top state)
  (vterm-state-scroll-top state))

(define (scroll-region-bottom state)
  (if (> (vterm-state-scroll-bottom state) -1)
      (vterm-state-scroll-bottom state)
      (vterm-state-rows state)))

(define (scroll-region-left state)
  (if (vterm-state-has-mode? state 'left-right-margin)
      (vterm-state-scroll-left state)
      0))

(define (scroll-region-right state)
  (if (and (vterm-state-has-mode? state 'left-right-margin)
           (> (vterm-state-scroll-right state) -1))
      (vterm-state-scroll-right state)
      (vterm-state-cols state)))

(define (send-scroll-event state start-col start-row end-col end-row downward rightward)
  (let ([handler (vterm-state-handler state)])
    (when handler
      (handler state
               (make-vterm-state-scroll-event
                 (make-vterm-rect start-col start-row)
                 (make-vterm-rect end-col end-row)
                 downward rightward)))))

(define (do-linefeed state)
  (cond
    [(= (vterm-state-cursor-row state)
        (- (scroll-region-bottom state) 1))
     #f ;; TODO send scroll event
    ]
    [(< (vterm-state-cursor-row state)
        (- (vterm-state-rows state) 1))
     (vterm-state-cursor-row-inc! state 1)
    ]))

(define (update-cursor state old-pos clean-phantom?)
  (when (not (vterm-pos=? old-pos (vterm-state-cursor state)))
    (when clean-phantom?
      (vterm-state-at-phantom?-set! state #f))
    ;; TODO send cursor event
  )
)

(define (handle-text state text)
  (define old-pos (make-vterm-pos (vterm-state-cursor-col state)
                                  (vterm-state-cursor-row state)))
  (define width 1)
  (for-each
    (lambda (ch)
      (when (or (vterm-state-at-phantom? state)
                (> (+ width (vterm-state-cursor-col state))
                   (vterm-state-current-row-width state)))
        (do-linefeed state)
        (vterm-state-cursor-col-set! state 0)
        (vterm-state-at-phantom?-set! state #f))
      ;; TODO handle insert mode, glyph event
      (if (>= (+ width (vterm-state-cursor-col state))
              (vterm-state-current-row-width state))
          (when (vterm-state-has-mode? state 'auto-wrap)
            (vterm-state-at-phantom?-set! state #t))
        (vterm-state-cursor-col-inc! state width)))
    (string->list text))
  (update-cursor state old-pos #f))

(define (do-tab state count dir)
  (let loop ([count count] [done? #f])
    (when (and (positive? count) (not done?))
      (cond
        ((fxpositive? dir)
         (if (< (vterm-state-cursor-col state)
                (- (vterm-state-current-row-width state) 1))
             (vterm-state-cursor-col-inc! state 1)
             (set! done? #t)))
        ((fxnegative? dir)
         (if (positive? (vterm-state-cursor-col state))
             (vterm-state-cursor-col-inc! state -1)
             (set! done? #t))))
      (when (is-col-tab-stop?
              (vterm-state-tab-stops state)
              (vterm-state-cursor-col state))
        (set! count (- count 1)))
      (loop count done?))))

(define (handle-ctl state code)
  (define old-pos (make-vterm-pos (vterm-state-cursor-col state)
                                  (vterm-state-cursor-row state)))
  (case code
    ;; bs
    ((#x8)
     (when (positive? (vterm-state-cursor-col state))
       (vterm-state-cursor-col-inc! state -1)))

    ;; ht
    ((#x9)
     (do-tab state 1 +1))

    ;; lf vt ff
    ((#xa #xb #xc)
     (do-linefeed state)
     (when (vterm-state-has-mode? state 'newline)
       (vterm-state-cursor-col-set! state 0)))

    ;; cr
    ((#xd)
     (vterm-state-cursor-col-set! state 0))

    ;; ls1
    ((#xe)
     (vterm-state-gl-set! state 1))

    ;; ls0
    ((#xf)
     (vterm-state-gl-set! state 0))

    ;; ind
    ((#x84)
     (do-linefeed state))

    ;; nel
    ((#x85)
     (do-linefeed state)
     (vterm-state-cursor-col-set! state 0))

    ;; hts
    ((#x88)
     (set-col-tab-stop (vterm-state-tab-stops state)
                       (vterm-state-cursor-col state)))

    ;; ri
    ((#x8d)
     (if (= (vterm-state-cursor-row state)
            (scroll-region-top state))
         #f ;; TODO send scroll event
         (when (positive? (vterm-state-cursor-row state))
           (vterm-state-cursor-row-inc! state -1))))

    ;; ss2
    ((#x8e)
     (vterm-state-gsingle-set! state 2))

    ;; ss3
    ((#x8f)
     (vterm-state-gsingle-set! state 3))
  )
  (update-cursor state old-pos #t))

(define (csi-arg-or args i def)
  (if (< i (length args))
      (let ([arg (list-ref args i)])
        (if arg
            (list-ref args i)
          def))
    def))

(define (csi-arg-count args i)
  (if (< i (length args))
      (let ([arg (list-ref args i)])
        (if (and arg (positive? arg)) arg 1))
    1))

(define (handle-csi state cmd args interm)
  (let ([leader (and (not (null? args))
                     (string? (car args))
                     (string-ref (car args) 0))])
    (call/cc
      (lambda (return)
        (when (and leader (not (member leader '(#\? #\>))))
          (return))
        (when (and interm
                   (or (not (= (string-length interm) 1))
                       (member (string-ref interm 0)
                               '(#\space #\" #\$ #\\))))
          (return))
        (let* ([leader-byte (if leader (char->integer leader) 0)]
               [interm-byte (if interm (char->integer (string-ref interm 0)) 0)]
               [command-byte (char->integer cmd)]
               [code (bitwise-ior (bitwise-arithmetic-shift interm-byte 16)
                                  (bitwise-arithmetic-shift leader-byte 8)
                                  command-byte)])
          (case code
            ((#x41) ;; CUU
             (let ([count (csi-arg-count args 0)])
               (vterm-state-cursor-row-inc! state (- count))
               (vterm-state-at-phantom?-set! state #f)))

            ((#x42) ;; CUD
             (let ([count (csi-arg-count args 0)])
               (vterm-state-cursor-row-inc! state count)
               (vterm-state-at-phantom?-set! state #f)))

            ((#x43) ;; CUF
             (let ([count (csi-arg-count args 0)])
               (vterm-state-cursor-col-inc! state count)
               (vterm-state-at-phantom?-set! state #f)))

            ((#x44) ;; CUB
             (let ([count (csi-arg-count args 0)])
               (vterm-state-cursor-col-inc! state (- count))
               (vterm-state-at-phantom?-set! state #f)))

            ((#x45) ;; CNL
             (let ([count (csi-arg-count args 0)])
               (vterm-state-cursor-col-set! state 0)
               (vterm-state-cursor-row-inc! state count)
               (vterm-state-at-phantom?-set! state #f)))

            ((#x46) ;; CPL
             (let ([count (csi-arg-count args 0)])
               (vterm-state-cursor-col-set! state 0)
               (vterm-state-cursor-row-inc! state (- count))
               (vterm-state-at-phantom?-set! state #f)))

            ((#x47) ;; CHA
             (let ([col (csi-arg-or args 0 1)])
               (vterm-state-cursor-col-set! state (- col 1))
               (vterm-state-at-phantom?-set! state #f)))

            ((#x48) ;; CUP
             (let ([row (csi-arg-or args 0 1)]
                   [col (csi-arg-or args 1 1)])
               (vterm-state-cursor-col-set! state (- col 1))
               (vterm-state-cursor-row-set! state (- row 1))
               (when (vterm-state-has-mode? state 'origin)
                 (vterm-state-cursor-row-inc! state (scroll-region-top state))
                 (vterm-state-cursor-col-inc! state (scroll-region-left state)))
               (vterm-state-at-phantom?-set! state #f)))
            ((#x49) ;; CHT
             (let ([count (csi-arg-count args 0)])
               (do-tab state count +1)))
            ((#x5a) ;; CBT
             (let ([count (csi-arg-count args 0)])
               (do-tab state count -1)))
            ((#x60) ;; HPA
             (let ([col (csi-arg-or args 0 1)])
               (vterm-state-cursor-col-set! state (- col 1))
               (vterm-state-at-phantom?-set! state #f)))
            ((#x61) ;; HPR
             (let ([col (csi-arg-count args 0)])
               (vterm-state-cursor-col-inc! state col)
               (vterm-state-at-phantom?-set! state #f)))
            ((#x64) ;; VPA
             (let ([row (csi-arg-or args 0 1)])
               (vterm-state-cursor-row-set! state (- row 1))
               (when (vterm-state-has-mode? state 'origin)
                 (vterm-state-cursor-row-inc! state (scroll-region-top state)))
               (vterm-state-at-phantom?-set! state #f)))
            ((#x65) ;; VPR
             (let ([count (csi-arg-count args 0)])
               (vterm-state-cursor-row-inc! state count)
               (vterm-state-at-phantom?-set! state #f)))
            ((#x66) ;; HVP
             (let ([row (csi-arg-or args 0 1)]
                   [col (csi-arg-or args 1 1)])
               (vterm-state-cursor-col-set! state (- col 1))
               (vterm-state-cursor-row-set! state (- row 1))
               (when (vterm-state-has-mode? state 'origin)
                 (vterm-state-cursor-row-inc! state (scroll-region-top state))
                 (vterm-state-cursor-col-inc! state (scroll-region-left state)))
               (vterm-state-at-phantom?-set! state #f)))
            ((#x67) ;; TBC
             (let ([val (csi-arg-or args 0 0)])
               (cond
                 ((eqv? val 0)
                  (clear-col-tab-stop (vterm-state-tab-stops state) (vterm-state-cursor-col state)))
                 ((or (eqv? val 3) (eqv? val 5))
                  (let loop ([i 0])
                    (when (< i val)
                      (clear-col-tab-stop (vterm-state-tab-stops state) i)
                      (loop (fx+ i 1))) )))))
            ((#x6a) ;; HPB
             (let ([count (csi-arg-count args 0)])
               (vterm-state-cursor-col-inc! state (- count))
               (vterm-state-at-phantom?-set! state #f)))
            ((#x6b) ;; VPB
             (let ([count (csi-arg-count args 0)])
               (vterm-state-cursor-row-inc! state (- count))
               (vterm-state-at-phantom?-set! state #f))) )

          ;; bounds check
          (let ([col (vterm-state-cursor-col state)]
                [row (vterm-state-cursor-row state)])
            (lbound row 0)
            (ubound row (- (vterm-state-rows state) 1))
            (vterm-state-cursor-row-set! state row)
 
            (lbound col 0)
            (ubound col (- (vterm-state-current-row-width state) 1))
            (vterm-state-cursor-col-set! state col))
        )))))

(define make-vterm-state
  (case-lambda
    [()
     (make-vterm-state #f)]

    [(handler)
     (let ([state (make-$vterm-state)])
       (vterm-state-handler-set! state handler)
       (vterm-state-parser-set! state
         (make-vterm-parser
           (lambda (seq)
             (cond
               ((string? seq) (handle-text state seq))
               ((list? seq)
                (case (car seq)
                  ((ctl) (handle-ctl state (cadr seq)))
                  ((csi) (handle-csi state (list-ref seq 1)
                                           (list-ref seq 2)
                                           (list-ref seq 3)))
                ))))))
       state)]))

(define (vterm-state-process state seq)
  (vterm-parse (vterm-state-parser state) seq))

)
