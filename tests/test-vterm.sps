#!/usr/bin/env scheme-script
;; -*- mode: scheme; coding: utf-8 -*- !#
;; Copyright (c) 2023 
;; SPDX-License-Identifier: MIT
#!r6rs

(import (rnrs (6))
        (srfi :64 testing)
        (vterm parser)
        (vterm state)
        (vterm encoding))

(define (display-cursor st)
  (display "curs:")(display (vterm-state-cursor-row st))
                   (display ", ")
                   (display (vterm-state-cursor-col st))
		   (newline))

(test-begin "vterm-seq->sexp")
(let ([p (make-vterm-parser)])
  (test-assert p)

  ;; control sequences
  ;; C0
  (test-equal '((ctl #x03)) (vterm-seq->sexp p #vu8(#x03)))
  (test-equal '((ctl #x1f)) (vterm-seq->sexp p #vu8(#x1f)))
  (test-equal '((ctl #x9f)) (vterm-seq->sexp p #vu8(#x9f)))
  (test-equal '((ctl #x07)) (vterm-seq->sexp p #vu8(#x07)))
  ;; C1 8bit
  (test-equal '((ctl #x83)) (vterm-seq->sexp p #vu8(#x83)))
  (test-equal '((ctl #x9f)) (vterm-seq->sexp p #vu8(#x9f)))
  ;; C1 7bit
  (test-equal '((ctl #x83)) (vterm-seq->sexp p #vu8(#x1b #x43)))
  (test-equal '((ctl #x9f)) (vterm-seq->sexp p #vu8(#x1b #x5f)))

  ;; Escape
  (test-equal '((esc "=")) (vterm-seq->sexp p "\x1b;="))
  ;; Escape 2-byte
  (test-equal '((esc "(X")) (vterm-seq->sexp p "\x1b;(X"))
  ;; Escape split write
  (test-assert (null? (vterm-seq->sexp p "\x1b;(")))
  (test-equal '((esc "(Y")) (vterm-seq->sexp p "Y"))
  ;; Escape cancels Escape, starts another
  (test-equal '((esc ")Z")) (vterm-seq->sexp p "\x1b;(\x1b;)Z"))
  ;; CAN cancels Escape, returns to normal mode
  (test-equal '("AB") (vterm-seq->sexp p "\x1b;(\x18;AB"))
  ;; C0 in Escape interrupts and continues
  (test-assert '((ctl 10) (esc "(X")) (vterm-seq->sexp p "\x1b;(\nX"))

  ;; Text
  (test-equal '("hello") (vterm-seq->sexp p "hello"))
  ;; Mixed sequences
  (test-equal `("1" (ctl ,(char->integer #\newline)) "2") (vterm-seq->sexp p "1\n2"))

  ;; CSI 0 args
  (test-equal '((csi #\a (#f) #f)) (vterm-seq->sexp p "\x1b;[a"))
  ;; CSI 1 arg
  (test-equal '((csi #\b (9) #f)) (vterm-seq->sexp p "\x1b;[9b"))
  ;; CSI 2 args
  (test-equal '((csi #\c (3 4) #f)) (vterm-seq->sexp p "\x1b;[3;4c"))
  ;; CSI 1 arg 1 sub
  (test-equal '((csi #\c (1 (2)) #f)) (vterm-seq->sexp p "\x1b;[1:2c"))
  ;; CSI many digits
  (test-equal '((csi #\d (678) #f)) (vterm-seq->sexp p "\x1b;[678d"))
  ;; CSI leading zero
  (test-equal '((csi #\e (7) #f)) (vterm-seq->sexp p "\x1b;[007e"))
  ;; CSI qmark
  (test-equal '((csi #\f ("?" 2 7) #f)) (vterm-seq->sexp p "\x1b;[?2;7f"))
  ;; CSI greater
  (test-equal '((csi #\c (">" #f) #f)) (vterm-seq->sexp p "\x1b;[>c"))
  ;; CSI SP
  (test-equal '((csi #\q (12) " ")) (vterm-seq->sexp p "\x1b;[12 q"))
  ;; Mixed CSI
  (test-equal '("A" (csi #\m (8) #f) "B") (vterm-seq->sexp p "A\x1b;[8mB"))
  ;; Split write
  (test-assert (null? (vterm-seq->sexp p "\x1b;")))
  (test-equal '((csi #\a (#f) #f)) (vterm-seq->sexp p "[a"))
  (test-equal '("foo") (vterm-seq->sexp p "foo\x1b;["))
  (test-equal '((csi #\b (4) #f)) (vterm-seq->sexp p "4b"))
  (test-assert (null? (vterm-seq->sexp p "\x1b;[12;")))
  (test-equal '((csi #\c (12 3) #f)) (vterm-seq->sexp p "3c"))
  ;; Escape cancels CSI, starts Escape
  (test-equal '((esc "9")) (vterm-seq->sexp p "\x1b;[123\x1b;9"))
  ;; CAN cancels CSI, returns to normal mode
  (test-equal '("AB") (vterm-seq->sexp p "\x1b;[12\x18;AB"))
  ;; C0 in Escape interrupts and continues
  (test-equal '((ctl 10) (csi #\X (12 3) #f)) (vterm-seq->sexp p "\x1b;[12\n;3X"))
  ;; OSC, DCS
  ;; OSC BEL
  (test-equal '((osc "1;Hello")) (vterm-seq->sexp p "\x1b;]1;Hello\x07;"))
  ;; OSC ST (7bit)
  (test-equal '((osc "1;Hello")) (vterm-seq->sexp p "\x1b;]1;Hello\x1b;\\"))
  ;; OSC ST (8bit)
  (test-equal '((osc "1;Hello")) (vterm-seq->sexp p "\x9d;1;Hello\x9c;"))
  ;; Escape cancels OSC, starts Escape
  (test-equal '((esc "9")) (vterm-seq->sexp p "\x1b;]Something\x1b;9"))
  ;; CAN cancels OSC, returns to normal mode
  (test-equal '("AB") (vterm-seq->sexp p "\x1b;]12\x18;AB"))
  ;; C0 in OSC interrupts and continues
  (test-equal '((ctl 10) (osc "2;Bye")) (vterm-seq->sexp p "\x1b;]2;\nBye\x07;"))
  ;; DCS BEL
  (test-equal '((dcs "Hello")) (vterm-seq->sexp p "\x1b;PHello\x07;"))
  ;; DCS ST (7bit)
  (test-equal '((dcs "Hello")) (vterm-seq->sexp p "\x1b;PHello\x1b;\\"))
  ;; DCS ST (8bit)
  (test-equal '((dcs "Hello")) (vterm-seq->sexp p "\x90;Hello\x9c;"))
  ;; Escape cancels DCS, starts Escape
  (test-equal '((esc "9")) (vterm-seq->sexp p "\x1b;PSomething\x1b;9"))
  ;; CAN cancels DCS, returns to normal mode
  (test-equal '("AB") (vterm-seq->sexp p "\x1b;P12\x18;AB"))
  ;; C0 in OSC interrupts and continues
  (test-equal '((ctl 10) (dcs "Bye")) (vterm-seq->sexp p "\x1b;PBy\ne\x07;"))
  ;; NUL ignored
  (test-assert (null? (vterm-seq->sexp p "\x00;")))

  ;; NUL ignored within CSI
  (test-equal '((csi #\m (123) #f)) (vterm-seq->sexp p "\x1b;[12\x00;3m"))
  ;; DEL ignored
  (test-assert (null? (vterm-seq->sexp p "\x7f;")))

  ;; DEL ignored within CSI
  (test-equal '((csi #\m (123) #f)) (vterm-seq->sexp p "\x1b;[12\x7f;3m"))
  ;; DEL inside text
  (test-equal '("AB" "C") (vterm-seq->sexp p "AB\x7f;C"))
)
(test-end)

(test-begin "sexp->vterm-seq")
(let ([p (make-vterm-parser)])
  (test-equal '((ctl 10)) (vterm-seq->sexp p (sexp->vterm-seq '((ctl 10))) ))
  (test-equal '((ctl #x83)) (vterm-seq->sexp p (sexp->vterm-seq '((ctl #x83))) ))
  (test-equal '((ctl 10) (ctl 10)) (vterm-seq->sexp p (sexp->vterm-seq '((ctl 10) (ctl 10))) ))
  (test-equal '((ctl 10) (ctl 12)) (vterm-seq->sexp p (sexp->vterm-seq '((ctl 10) (ctl 12))) ))
  (test-equal '((esc "=")) (vterm-seq->sexp p (sexp->vterm-seq '((esc "="))) ))
  (test-equal '((ctl 10) (esc "=")) (vterm-seq->sexp p (sexp->vterm-seq '((ctl 10) (esc "="))) ))
  (test-equal '((ctl 10) (esc "=") (ctl #x83)) (vterm-seq->sexp p (sexp->vterm-seq '((ctl 10) (esc "=") (ctl #x83))) ))
  (test-equal '("hello") (vterm-seq->sexp p (sexp->vterm-seq '("hello")) ))
  (test-equal '("1" (ctl 10) "2") (vterm-seq->sexp p (sexp->vterm-seq '("1" (ctl 10) "2")) ))
  (test-equal '((csi #\m (1 2) #f)) (vterm-seq->sexp p (sexp->vterm-seq '((csi #\m (1 2)) ))))
  (test-equal '((csi #\m (1 (2)) #f)) (vterm-seq->sexp p (sexp->vterm-seq '((csi #\m (1 (2))) ))))
  (test-equal '((csi #\c (#f) #f)) (vterm-seq->sexp p (sexp->vterm-seq '((csi #\c))) ))
  (test-equal '((csi #\u (1) #f)) (vterm-seq->sexp p (sexp->vterm-seq '((csi #\u (1)))) ))
  (test-equal '((csi #\u (1) "+")) (vterm-seq->sexp p (sexp->vterm-seq '((csi #\u (1) "+"))) ))
  (test-equal '((osc "hello")) (vterm-seq->sexp p (sexp->vterm-seq '((osc "hello")) ) ))
  (test-equal '((dcs "set")) (vterm-seq->sexp p (sexp->vterm-seq '((dcs "set")) ) ))
)
(test-end)

(test-begin "state-cursor basic")
(let* ([st (make-vterm-state)])
  ;; implicit
  (vterm-state-process st "ABC")
  (test-equal 3 (vterm-state-cursor-col st))
  (test-equal 0 (vterm-state-cursor-row st))

  ;; backspace
  (vterm-state-process st (make-string 1 #\backspace))
  (test-equal 2 (vterm-state-cursor-col st))
  (test-equal 0 (vterm-state-cursor-row st))

  ;; horizontal tab
  (vterm-state-process st "\t")
  (test-equal 8 (vterm-state-cursor-col st))
  (test-equal 0 (vterm-state-cursor-row st))

  ;; carriage return
  (vterm-state-process st "\r")
  (test-equal 0 (vterm-state-cursor-col st))
  (test-equal 0 (vterm-state-cursor-row st))

  ;; linefeed
  (vterm-state-process st "\n")
  (test-equal 0 (vterm-state-cursor-col st))
  (test-equal 1 (vterm-state-cursor-row st))

  ;; backspace bounded by lefthand edge
  (vterm-state-process st "\x1b;[4;2H")
  (test-equal 1 (vterm-state-cursor-col st))
  (test-equal 3 (vterm-state-cursor-row st))
  (vterm-state-process st (make-string 1 #\backspace))
  (test-equal 0 (vterm-state-cursor-col st))
  (test-equal 3 (vterm-state-cursor-row st))
  (vterm-state-process st (make-string 1 #\backspace))
  (test-equal 0 (vterm-state-cursor-col st))
  (test-equal 3 (vterm-state-cursor-row st))

  ;; backspace cancels phantom
  (vterm-state-process st "\x1b;[4;80H")
  (test-equal 79 (vterm-state-cursor-col st))
  (test-equal 3 (vterm-state-cursor-row st))
  (vterm-state-process st "X")
  (test-equal 79 (vterm-state-cursor-col st))
  (test-equal 3 (vterm-state-cursor-row st))
  (vterm-state-process st (make-string 1 #\backspace))
  (test-equal 78 (vterm-state-cursor-col st))
  (test-equal 3 (vterm-state-cursor-row st))

  ;; HT bounded by righthand edge
  (vterm-state-process st "\x1b;[1;78H")
  (test-equal 77 (vterm-state-cursor-col st))
  (test-equal 0 (vterm-state-cursor-row st))
  (vterm-state-process st "\t")
  (test-equal 79 (vterm-state-cursor-col st))
  (test-equal 0 (vterm-state-cursor-row st))
  (vterm-state-process st "\t")
  (test-equal 79 (vterm-state-cursor-col st))
  (test-equal 0 (vterm-state-cursor-row st))
)
(test-end)

(test-begin "state-cursor index, reverse-index")
(let* ([st (make-vterm-state)])
  ;; index
  (vterm-state-process st "ABC\x1b;D")
  (test-equal 3 (vterm-state-cursor-col st))
  (test-equal 1 (vterm-state-cursor-row st))
  ;; reverse-index
  (vterm-state-process st "\x1b;M")
  (test-equal 3 (vterm-state-cursor-col st))
  (test-equal 0 (vterm-state-cursor-row st))
  ;; new line
  (vterm-state-process st "\x1b;E")
  (test-equal 0 (vterm-state-cursor-col st))
  (test-equal 1 (vterm-state-cursor-row st))
)
(test-end)

(test-begin "state-cursor movement")
(let* ([st (make-vterm-state)])
  ;; cursor down
  (vterm-state-process st "\x1b;[B")
  (test-equal 0 (vterm-state-cursor-col st))
  (test-equal 1 (vterm-state-cursor-row st))
  (vterm-state-process st "\x1b;[3B")
  (test-equal 0 (vterm-state-cursor-col st))
  (test-equal 4 (vterm-state-cursor-row st))
  (vterm-state-process st "\x1b;[0B")
  (test-equal 0 (vterm-state-cursor-col st))
  (test-equal 5 (vterm-state-cursor-row st))
  ;; cursor forward
  (vterm-state-process st "\x1b;[C")
  (test-equal 1 (vterm-state-cursor-col st))
  (test-equal 5 (vterm-state-cursor-row st))
  (vterm-state-process st "\x1b;[3C")
  (test-equal 4 (vterm-state-cursor-col st))
  (test-equal 5 (vterm-state-cursor-row st))
  (vterm-state-process st "\x1b;[0C")
  (test-equal 5 (vterm-state-cursor-col st))
  (test-equal 5 (vterm-state-cursor-row st))
  ;; cursor up
  (vterm-state-process st "\x1b;[A")
  (test-equal 5 (vterm-state-cursor-col st))
  (test-equal 4 (vterm-state-cursor-row st))
  (vterm-state-process st "\x1b;[3A")
  (test-equal 5 (vterm-state-cursor-col st))
  (test-equal 1 (vterm-state-cursor-row st))
  (vterm-state-process st "\x1b;[0A")
  (test-equal 5 (vterm-state-cursor-col st))
  (test-equal 0 (vterm-state-cursor-row st))
  ;; cursor backward
  (vterm-state-process st "\x1b;[D")
  (test-equal 4 (vterm-state-cursor-col st))
  (test-equal 0 (vterm-state-cursor-row st))
  (vterm-state-process st "\x1b;[3D")
  (test-equal 1 (vterm-state-cursor-col st))
  (test-equal 0 (vterm-state-cursor-row st))
  (vterm-state-process st "\x1b;[0D")
  (test-equal 0 (vterm-state-cursor-col st))
  (test-equal 0 (vterm-state-cursor-row st))
  ;; cursor next line
  (vterm-state-process st "   ")
  (test-equal 3 (vterm-state-cursor-col st))
  (test-equal 0 (vterm-state-cursor-row st))
  (vterm-state-process st "\x1b;[E")
  (test-equal 0 (vterm-state-cursor-col st))
  (test-equal 1 (vterm-state-cursor-row st))
  (vterm-state-process st "   ")
  (test-equal 3 (vterm-state-cursor-col st))
  (test-equal 1 (vterm-state-cursor-row st))
  (vterm-state-process st "\x1b;[2E")
  (test-equal 0 (vterm-state-cursor-col st))
  (test-equal 3 (vterm-state-cursor-row st))
  (vterm-state-process st "\x1b;[0E")
  (test-equal 0 (vterm-state-cursor-col st))
  (test-equal 4 (vterm-state-cursor-row st))
  ;; cursor previous line
  (vterm-state-process st "   ")
  (test-equal 3 (vterm-state-cursor-col st))
  (test-equal 4 (vterm-state-cursor-row st))
  (vterm-state-process st "\x1b;[F")
  (test-equal 0 (vterm-state-cursor-col st))
  (test-equal 3 (vterm-state-cursor-row st))
  (vterm-state-process st "   ")
  (test-equal 3 (vterm-state-cursor-col st))
  (test-equal 3 (vterm-state-cursor-row st))
  (vterm-state-process st "\x1b;[2F")
  (test-equal 0 (vterm-state-cursor-col st))
  (test-equal 1 (vterm-state-cursor-row st))
  (vterm-state-process st "\x1b;[0F")
  (test-equal 0 (vterm-state-cursor-col st))
  (test-equal 0 (vterm-state-cursor-row st))
  ;; cursor horizontal absolute
  (vterm-state-process st "\n")
  (test-equal 0 (vterm-state-cursor-col st))
  (test-equal 1 (vterm-state-cursor-row st))
  (vterm-state-process st "\x1b;[20G")
  (test-equal 19 (vterm-state-cursor-col st))
  (test-equal 1 (vterm-state-cursor-row st))
  (vterm-state-process st "\x1b;[G")
  (test-equal 0 (vterm-state-cursor-col st))
  (test-equal 1 (vterm-state-cursor-row st))
  ;; cursor position
  (vterm-state-process st "\x1b;[10;5H")
  (test-equal 4 (vterm-state-cursor-col st))
  (test-equal 9 (vterm-state-cursor-row st))
  (vterm-state-process st "\x1b;[8H")
  (test-equal 0 (vterm-state-cursor-col st))
  (test-equal 7 (vterm-state-cursor-row st))
  (vterm-state-process st "\x1b;[H")
  (test-equal 0 (vterm-state-cursor-col st))
  (test-equal 0 (vterm-state-cursor-row st))
  ;; cursor position cancels phantom
  (vterm-state-process st "\x1b;[10;78H")
  (test-equal 77 (vterm-state-cursor-col st))
  (test-equal 9 (vterm-state-cursor-row st))
)
(test-end)

(test-begin "cursor bounds checking")
(let* ([st (make-vterm-state)])
  (vterm-state-process st "\x1b;[A")
  (test-equal 0 (vterm-state-cursor-col st))
  (test-equal 0 (vterm-state-cursor-row st))
  (vterm-state-process st "\x1b;[D")
  (test-equal 0 (vterm-state-cursor-col st))
  (test-equal 0 (vterm-state-cursor-row st))
  (vterm-state-process st "\x1b;[25;80H")
  (test-equal 79 (vterm-state-cursor-col st))
  (test-equal 24 (vterm-state-cursor-row st))
  (vterm-state-process st "\x1b;[B")
  (test-equal 79 (vterm-state-cursor-col st))
  (test-equal 24 (vterm-state-cursor-row st))
  (vterm-state-process st "\x1b;[C")
  (test-equal 79 (vterm-state-cursor-col st))
  (test-equal 24 (vterm-state-cursor-row st))
  (vterm-state-process st "\x1b;[E")
  (test-equal 0 (vterm-state-cursor-col st))
  (test-equal 24 (vterm-state-cursor-row st))
  (vterm-state-process st "\x1b;[H")
  (test-equal 0 (vterm-state-cursor-col st))
  (test-equal 0 (vterm-state-cursor-row st))
  (vterm-state-process st "\x1b;[F")
  (test-equal 0 (vterm-state-cursor-col st))
  (test-equal 0 (vterm-state-cursor-row st))
  (vterm-state-process st "\x1b;[999G")
  (test-equal 79 (vterm-state-cursor-col st))
  (test-equal 0 (vterm-state-cursor-row st))
  (vterm-state-process st "\x1b;[99;99H")
  (test-equal 79 (vterm-state-cursor-col st))
  (test-equal 24 (vterm-state-cursor-row st))
)
(test-end)

(test-begin "horizontal/vertical positioning")
(let* ([st (make-vterm-state)])
  ;; horizontal position absolute
  (vterm-state-process st "\x1b;[5`")
  (test-equal 4 (vterm-state-cursor-col st))
  (test-equal 0 (vterm-state-cursor-row st))
  ;; horizontal position relative
  (vterm-state-process st "\x1b;[3a")
  (test-equal 7 (vterm-state-cursor-col st))
  (test-equal 0 (vterm-state-cursor-row st))
  ;; horizontal position backward
  (vterm-state-process st "\x1b;[3j")
  (test-equal 4 (vterm-state-cursor-col st))
  (test-equal 0 (vterm-state-cursor-row st))
  ;; horizontal and vertical position
  (vterm-state-process st "\x1b;[3;3f")
  (test-equal 2 (vterm-state-cursor-col st))
  (test-equal 2 (vterm-state-cursor-row st))
  ;; vertical position absolute
  (vterm-state-process st "\x1b;[5d")
  (test-equal 2 (vterm-state-cursor-col st))
  (test-equal 4 (vterm-state-cursor-row st))
  ;; vertical position relative
  (vterm-state-process st "\x1b;[2e")
  (test-equal 2 (vterm-state-cursor-col st))
  (test-equal 6 (vterm-state-cursor-row st))
  ;; vertical position backward
  (vterm-state-process st "\x1b;[2k")
  (test-equal 2 (vterm-state-cursor-col st))
  (test-equal 4 (vterm-state-cursor-row st))
)
(test-end)

(test-begin "tabs movement")
(let* ([st (make-vterm-state)])
  ;; horizontal tab
  (vterm-state-process st "\t")
  (test-equal 8 (vterm-state-cursor-col st))
  (test-equal 0 (vterm-state-cursor-row st))
  (vterm-state-process st "   ")
  (test-equal 11 (vterm-state-cursor-col st))
  (test-equal 0 (vterm-state-cursor-row st))
  (vterm-state-process st "\t")
  (test-equal 16 (vterm-state-cursor-col st))
  (test-equal 0 (vterm-state-cursor-row st))
  (vterm-state-process st "       ")
  (test-equal 23 (vterm-state-cursor-col st))
  (test-equal 0 (vterm-state-cursor-row st))
  (vterm-state-process st "\t")
  (test-equal 24 (vterm-state-cursor-col st))
  (test-equal 0 (vterm-state-cursor-row st))
  (vterm-state-process st "        ")
  (test-equal 32 (vterm-state-cursor-col st))
  (test-equal 0 (vterm-state-cursor-row st))
  (vterm-state-process st "\t")
  (test-equal 40 (vterm-state-cursor-col st))
  (test-equal 0 (vterm-state-cursor-row st))
  ;; cursor horizontal tab
  (vterm-state-process st "\x1b;[I")
  (test-equal 48 (vterm-state-cursor-col st))
  (test-equal 0 (vterm-state-cursor-row st))
  (vterm-state-process st "\x1b;[2I")
  (test-equal 64 (vterm-state-cursor-col st))
  (test-equal 0 (vterm-state-cursor-row st))
  ;; cursor backward tab
  (vterm-state-process st "\x1b;[Z")
  (test-equal 56 (vterm-state-cursor-col st))
  (test-equal 0 (vterm-state-cursor-row st))
  (vterm-state-process st "\x1b;[2Z")
  (test-equal 40 (vterm-state-cursor-col st))
  (test-equal 0 (vterm-state-cursor-row st))
)
(test-end)

(test-begin "encoding ascii")
(let ([dec (make-ascii-decoder)])
  (let-values ([(cpl bcount) (decode-text dec #vu8(#x31 #x32))])
    (test-equal '(#x31 #x32) cpl)
    (test-equal 2 bcount) ))

(test-end)

(exit (if (zero? (test-runner-fail-count (test-runner-get))) 0 1))
