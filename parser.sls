;; -*- mode: scheme; coding: utf-8 -*-
;; Copyright (c) 2023 
;; SPDX-License-Identifier: MIT
#!r6rs

(library (vterm parser)
  (export
    make-vterm-parser
    vterm-seq->sexp
    sexp->vterm-seq
    vterm-parse)
  (import (rnrs)
          (rnrs mutable-pairs)
          (rnrs mutable-strings)
          (vterm encoding))

(define latin-1-transcoder (make-transcoder (latin-1-codec) 'none 'ignore))

(define ascii-decoder (make-ascii-decoder))

(define-record-type vterm-parser
  (fields
    (mutable state) ;; normal, csi-leader, csi-args, csi-intermed, esc, string, esc-in-string
    (mutable intermed)
    (mutable intermed-len)
    (mutable csi-leader)
    (mutable csi-leader-len)
    (mutable csi-all-args)
    (mutable csi-sub-args)
    (mutable string-type) ;; osc, dcs
    (mutable string-buf)
    (mutable string-len)
    (mutable utf8-enabled? vterm-parser-utf8-enabled? vterm-parser-utf8-enable!)
    handler ;; (string-or-sexp)
    (mutable seq-list)
    (mutable decoder))
  (protocol
    (lambda (new)
      (letrec ([mk-vterm-parser
                 (case-lambda
                   [(handler)
                    (new 'normal
                         (make-bytevector 16) 0
                         (make-bytevector 16) 0
                         #f #f
                         'osc
                         (make-bytevector 64) 0
                         #f
                         handler
                         '()
                         ascii-decoder)]
                   [()
                    (mk-vterm-parser #f)])
               ])
        mk-vterm-parser))))

(define (get-input-bytes input)
  (cond
    [(bytevector? input) input]
    [(string? input) (string->bytevector input latin-1-transcoder)]
    [(fixnum? input) (make-bytevector 1 input)]
    [else (error 'vterm-parser-write! "Invalid input value")])
)

(define (make-ctl-seq val)
  (list 'ctl val) )

(define (make-esc-seq val)
  (list 'esc val) )

(define (make-csi-seq leader args intermed cmd)
  (list 'csi cmd (append (if leader (list leader) '()) args) intermed) )

(define (make-dcs-seq val)
  (list 'dcs val) )

(define (make-osc-seq val)
  (list 'osc val) )

(define (vterm-parse parser bytes)
  (define input (get-input-bytes bytes))
  (define len (bytevector-length input))
  (define string-pos #f)

  (define (intermed? val)
    (and (>= val #x20)
         (<= val #x2f))
  )

  (define (enter-state st)
    (vterm-parser-state-set! parser st)
    (set! string-pos #f)
  )

  (define (enter-string-state pos)
    (vterm-parser-state-set! parser 'string)
    (set! string-pos (+ pos 1))
  )

  (define (enter-normal-state)
    (enter-state 'normal)
  )

  (define (get-state)
    (vterm-parser-state parser)
  )

  (define (any-string-state?)
    (memq (get-state) (list 'string 'esc-in-string))
  )

  (define (append-string-buf pos len)
    (define string-buf (vterm-parser-string-buf parser))
    (define string-len (vterm-parser-string-len parser))

    ;; truncate the len
    (when (> len (- (bytevector-length string-buf) string-len))
      (set! len (- (bytevector-length string-buf) string-len)) )

    (when (positive? len)
      (bytevector-copy! input pos string-buf string-len len)
      (vterm-parser-string-len-set! parser (+ string-len len)) )
  )

  (define (more-string len)
    (append-string-buf string-pos len)
  )

  (define (done-string len)
    (let ([str ""])
      (when string-pos
        (append-string-buf string-pos len))
      (let* ([str-len (vterm-parser-string-len parser)]
             [bv (make-bytevector (vterm-parser-string-len parser))]
             [sb (vterm-parser-string-buf parser)])
        (bytevector-copy! sb 0 bv 0 str-len)
        (set! str (bytevector->string bv latin-1-transcoder)) )
      (case (vterm-parser-string-type parser)
        [(osc) (do-osc str)]
        [(dcs) (do-dcs str)] ))
  )

  (define (start-string type)
    (vterm-parser-string-type-set! parser type)
    (vterm-parser-string-len-set! parser 0)
  )

  (define (add-seq-entry seq)
    (vterm-parser-seq-list-set! parser
                                (append (vterm-parser-seq-list parser)
                                        (list seq)))
  )

  (define (do-control val)
    (cond [(vterm-parser-handler parser) =>
           (lambda (handler)
             (handler (make-ctl-seq val)) )]
          [else (add-seq-entry (make-ctl-seq val))])
  )

  (define (do-escape val)
    (let ([bv (make-bytevector (+ (vterm-parser-intermed-len parser) 1))])
      (bytevector-copy! (vterm-parser-intermed parser) 0
                        bv 0
                        (vterm-parser-intermed-len parser))
      (bytevector-u8-set! bv (vterm-parser-intermed-len parser) val)
      (cond [(vterm-parser-handler parser) =>
             (lambda (handler)
               (handler (make-esc-seq (bytevector->string bv latin-1-transcoder))))]
            [else (add-seq-entry (make-esc-seq (bytevector->string bv latin-1-transcoder)))])
      )
  )

  (define (do-csi cmd)
    (define leader-len (vterm-parser-csi-leader-len parser))
    (define leader (vterm-parser-csi-leader parser))
    (define args (vterm-parser-csi-all-args parser))
    (define intermed-len (vterm-parser-intermed-len parser))
    (define intermed (vterm-parser-intermed parser))
    
    (let ([handler (vterm-parser-handler parser)]
          [leader-bv (if (positive? leader-len) (make-bytevector leader-len) #f)]
          [leader-str #f]
          [intermed-bv (if (positive? intermed-len) (make-bytevector intermed-len) #f)]
          [intermed-str #f])
      (when (positive? leader-len)
        (bytevector-copy! leader 0 leader-bv 0 leader-len)
        (set! leader-str (bytevector->string leader-bv latin-1-transcoder)) )
      (when (positive? intermed-len)
        (bytevector-copy! intermed 0 intermed-bv 0 intermed-len)
        (set! intermed-str (bytevector->string intermed-bv latin-1-transcoder)) )
      (if handler
          (handler (make-csi-seq leader-str args intermed-str (integer->char cmd)))
          ;; else
          (add-seq-entry (make-csi-seq leader-str
                                       args
                                       intermed-str
                                       (integer->char cmd))) )
    )
  )

  (define (do-osc str)
    (cond [(vterm-parser-handler parser) =>
           (lambda (handler)
             (handler (make-osc-seq str)))]
          [else (add-seq-entry (make-osc-seq str))])
  )

  (define (do-dcs str)
    (cond [(vterm-parser-handler parser) =>
           (lambda (handler)
             (handler (make-dcs-seq str)))]
          [else (add-seq-entry (make-dcs-seq str))])
  )

  (define (do-text bytes pos len)
    (define handler (vterm-parser-handler parser))
    (define decoder (vterm-parser-decoder parser))

    (let-values ([(cpl bc) (decode-text decoder bytes pos len)])
      (let ([str (list->string (map integer->char cpl))])
        (when (and str (positive? (string-length str)))
          (if handler
              (handler str)
              ;; else
              (add-seq-entry str))))
      bc))

  (define (csi-all-args)
    (vterm-parser-csi-all-args parser)
  )
  (define (csi-all-args-set! args)
    (vterm-parser-csi-all-args-set! parser args)
  )

  (define (csi-sub-args)
    (vterm-parser-csi-sub-args parser)
  )
  (define (csi-sub-args-set! args)
    (vterm-parser-csi-sub-args-set! parser args)
  )

  (define (csi-args-list)
    (or (csi-sub-args) (csi-all-args))
  )

  (define (csi-args-init!)
    (csi-all-args-set! (list #f))
    (csi-sub-args-set! #f)
    (vterm-parser-state-set! parser 'csi-args)
  )

  (define (csi-arg-get)
    (list-ref (csi-args-list)
              (- (length (csi-args-list))
                 1))
  )

  (define (csi-arg-set! val)
    (set-car! (list-tail (csi-args-list)
                         (- (length (csi-args-list)) 1))
              val)
  )

  (define (csi-arg-add! val)
    (let ([lst (append (csi-args-list) (list val))])
      (if (csi-sub-args)
          (csi-sub-args-set! lst)
          ;; else
          (csi-all-args-set! lst) ))
  )

  (case (get-state)
    [(string esc-in-string) (set! string-pos 0)]
    [else (set! string-pos #f)])

  (let loop-bytes ([pos 0])
    (when (< pos len)
      (call/cc
        (lambda (continue)
          (let ([val (bytevector-u8-ref input pos)])
            (cond
              ;; NUL, DEL
              [(memq val (list #x00 #x7f))
               (when (any-string-state?)
                 (more-string (- pos string-pos))
                 (set! string-pos (+ pos 1)) )
               (continue)]

              ;; CAN, SUB
              [(memq val (list #x18 #x1a))
               (enter-normal-state)
               (continue)]

              ;; ESC
              [(= val #x1b)
               (vterm-parser-intermed-len-set! parser 0)
               (if (eq? 'string (get-state))
                   (vterm-parser-state-set! parser 'esc-in-string)
                   ;; else
                   (enter-state 'esc))
               (continue)]

              ;; BEL, can stand for ST in OSC or DCS state
              [(and (= val #x07) (eq? 'string (get-state)))]

              ;; other C0
              [(< val #x20)
               (when (any-string-state?)
                 (more-string (- pos string-pos)) )
               (do-control val)
               (when (any-string-state?)
                 (set! string-pos (+ pos 1)) )
               (continue) ])

            ;; Process states

            (case (get-state)
              [(esc-in-string)
               (when (= val #x5c)
                 (vterm-parser-state-set! parser 'string)
                 (done-string (- pos string-pos 1))
                 (enter-normal-state)
                 (continue)) 
               ;; fall through the next states
               (vterm-parser-state-set! parser 'esc) ])

            ;; ESC
            (case (get-state)
              [(esc)
               (case val
                 ;; DCS
                 [(#x50)
                  (start-string 'dcs)
                  (enter-string-state pos)]
                 ;; CSI
                 [(#x5b)
                  (vterm-parser-csi-leader-len-set! parser 0)
                  (enter-state 'csi-leader)]
                 ;; OSC
                 [(#x5d)
                  (start-string 'osc)
                  (enter-string-state pos)]
                 [else
                   (cond
                     [(intermed? val)
                      (when (< (vterm-parser-intermed-len parser) (- 16 1))
                        (bytevector-u8-set! (vterm-parser-intermed parser)
                                            (vterm-parser-intermed-len parser)
                                            val)
                        (vterm-parser-intermed-len-set! parser (+ (vterm-parser-intermed-len parser) 1)))
                        (continue)]
                     [(= val #x1b)]
                     [(and (= (vterm-parser-intermed-len parser) 0)
                           (>= val #x40)
                           (<  val #x60))
                      (do-control (+ val #x40))
                      (enter-normal-state)]
                     [(and (>= val #x30)
                           (<  val #x7f))
                      (do-escape val)
                      (enter-normal-state)]
                     [else "Unhandled byte in ESC"])
                 ])
              (continue)])

            ;; CSI
            (case (get-state)
              [(csi-leader)
               (when (and (>= val #x3c)
                          (<= val #x3f))
                 (when (< (vterm-parser-csi-leader-len parser) (- 16 1))
                   (bytevector-u8-set! (vterm-parser-csi-leader parser)
                                       (vterm-parser-csi-leader-len parser)
                                       val)
                   (vterm-parser-csi-leader-len-set! parser (+ (vterm-parser-csi-leader-len parser) 1)))
                 (continue))
               ;; else fallthrough
               (csi-args-init!) ])
            ;; CSI ARGS
            (case (get-state)
              [(csi-args)
               ;; numerical value of argument
               (when (and (>= val (char->integer #\0))
                          (<= val (char->integer #\9)))
                 (when (not (csi-arg-get))
                   (csi-arg-set! 0) )
                 (csi-arg-set! (* (csi-arg-get) 10))
                 (csi-arg-set! (+ (csi-arg-get)
                                  (- val (char->integer #\0))))
                 (continue))
               (when (= val (char->integer #\:))
                 (when (not (csi-sub-args))
                   (let ([sub-args (list)])
                     (csi-sub-args-set! sub-args)))
                 (csi-arg-add! #f)
                 (continue))
               (when (= val (char->integer #\;))
                 (when (csi-sub-args)
                   (let ([sub-args (csi-sub-args)])
                     (csi-sub-args-set! #f)
                     (csi-arg-add! sub-args) ))
                 (csi-arg-add! #f)
                 (continue))
               ;; else fallthrough
               (when (csi-sub-args)
                   (let ([sub-args (csi-sub-args)])
                     (csi-sub-args-set! #f)
                     (csi-arg-add! sub-args) ))
               (vterm-parser-intermed-len-set! parser 0)
               (vterm-parser-state-set! parser 'csi-intermed) ])
            ;; CSI INTERMED
            (case (get-state)
              [(csi-intermed)
               (cond [(intermed? val)
                      (when (< (vterm-parser-intermed-len parser) (- 16 1))
                        (bytevector-u8-set! (vterm-parser-intermed parser)
                                            (vterm-parser-intermed-len parser)
                                            val)
                        (vterm-parser-intermed-len-set! parser (+ (vterm-parser-intermed-len parser) 1)))
                      (continue)]
                     ;; ESC in CSI cancels
                     [(= val #x1b)]
                     [(and (>= val #x40) (<= val #x7e))
                      (do-csi val)])
               (enter-normal-state)
               (continue) ])
            ;; STRING, NORMAL, TEXT
            (case (get-state)
              [(string)
               (when (or (= val #x07)
                         (and (= val #x9c)
                              (not (vterm-parser-utf8-enabled? parser))))
                 (done-string (- pos string-pos))
                 (enter-normal-state) )]
              [(normal)
               (cond [(and (>= val #x80) (< val #xa0)
                           (not (vterm-parser-utf8-enabled? parser)))
                      (case val
                        [(#x90)
                         (start-string 'dcs)
                         (enter-string-state pos)]
                        [(#x9b)
                         (enter-state 'csi-leader)]
                        [(#x9d)
                         (start-string 'osc)
                         (enter-string-state pos)]
                        [else
                         (do-control val)]
                      )]
                     ;; TEXT
                     [else
                      (let ([eaten (do-text input pos (- len pos))])
                        (when (= eaten 0)
                          (set! eaten 1) )
                        (set! pos (+ pos (- eaten 1))) )]
               )])
      )))
      (loop-bytes (+ pos 1))
    ))
    (let ([ls (vterm-parser-seq-list parser)])
      (vterm-parser-seq-list-set! parser '())
      ls)
)

(define (vterm-seq->sexp parser bytes)
  (vterm-parse parser bytes)
)

(define sexp->vterm-seq
  (case-lambda
    [(sexp)
     (define ctl-mode-bit 7)

     (define (encode-bytes byte/bv)
       (cond [(number? byte/bv) (bytevector->string (make-bytevector 1 byte/bv) latin-1-transcoder)]
             [(bytevector? byte/bv) (bytevector->string byte/bv latin-1-transcoder)] )
     )

     (define (encode-ctl ls)
       (let ([code (list-ref ls 1)])
         (if (and (>= code 128) (= ctl-mode-bit 7))
             (string-append "\x1b;" (encode-bytes (- code #x40)))
             ;; else
             (encode-bytes code)) )
     )

     (define (encode-esc ls)
       (let ([str (list-ref ls 1)])
         (string-append "\x1b;" str) )
     )

     ;; can be in one of the form:
     ;; (csi cmd)
     ;; (csi cmd args)
     ;; (csi cmd args intermed)
     (define (encode-csi ls)
       (let ([str "\x1b;["]
             [len (length ls)]
             [cmd #f]
             [args #f]
             [leader #f]
             [intermed #f])
         (when (>= len 2)
           (set! cmd (list-ref ls 1)) )
         (when (>= len 3)
           (set! args (list-ref ls 2)) )
         (when (= len 4)
           (set! intermed (list-ref ls 3)) )
         (when (not cmd)
           (error 'encode-csi "csi requires at least command code") )
         (let loop-args ([args args] [i 0])
           (if (and args (not (null? args)))
             (let ([a (car args)])
               (when (and (> i 0)
                          (not (string? a))
                          (not (list? a)))
                 (set! str (string-append str ";")) )
               (cond [(number? a)
                      (set! str (string-append str (number->string a)))]
                     [(list? a)
                      (set! str (string-append str
                                               (apply string-append (map  (lambda (n)
                                                                            (string-append ":" (number->string n)))
                                                                          a)))) ]
                     [(string? a)
                      (set! str (string-append str a))] )
               (loop-args (cdr args) (+ i 1)) )))
         (when intermed
           (set! str (string-append str intermed)) )
         (string-append str (make-string 1 cmd)) )
     )

     (define (encode-osc ls)
       (let ([str (list-ref ls 1)])
         (string-append "\x1b;]" str "\x07;") )
     )

     (define (encode-dcs ls)
       (let ([str (list-ref ls 1)])
         (string-append "\x1b;P" str "\x07;") )
     )

     (let loop ([ls sexp][str ""])
       (if (not (null? ls))
           (let*([seq (car ls)]
                 [seq-type (if (not (string? seq)) (car seq) #f)]
                 [enc-str
                   (cond
                     [(string? seq) seq]
                     [(eq? seq-type 'ctl) (encode-ctl seq)]
                     [(eq? seq-type 'esc) (encode-esc seq)]
                     [(eq? seq-type 'csi) (encode-csi seq)]
                     [(eq? seq-type 'osc) (encode-osc seq)]
                     [(eq? seq-type 'dcs) (encode-dcs seq)]
                     [else (error 'sexp->vterm-seq "Invalid sequence type")]
                   )
                 ])
             (loop (cdr ls) (string-append str enc-str)) )
           ;; else
           str) )]
  )
)

)
